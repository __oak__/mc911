import pprint
import argparse
from collections import defaultdict
from codegen import lya_compile

Index = {}
sp = -1
pc = 0
D = [0]*1000
M = [None]*1000

def Ldc(k): # Load constant
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = k

def Ldv(i, j): # Load value
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = M[D[i] + j]

def Ldr(i, j): # Load reference
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = D[i] + j

def Stv(i, j): # Store value
  global sp, pc, Index, D, H, M
  M[D[i] + j] = M[sp]
  sp = sp - 1

def Lrv(i, j): # Load reference value
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = M[M[D[i] + j]]

def Srv(i, j): # Store reference value
  global sp, pc, Index, D, H, M
  M[M[D[i] + j]] = M[sp]
  sp = sp - 1

def Add(): # Add
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] + M[sp]
  sp = sp - 1

def Sub(): # Subtract
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] - M[sp]
  sp = sp - 1

def Mul(): # Multiply
  global sp, pc, Index, D, H, M
  M[sp - 1] = int(M[sp - 1] * M[sp])
  sp = sp - 1

def Div(): # Division
  global sp, pc, Index, D, H, M
  M[sp - 1] = int(M[sp - 1] / M[sp])
  sp = sp - 1

def Mod(): # Modulus
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] % M[sp]
  sp = sp - 1

def Neg(): # Negate
  global sp, pc, Index, D, H, M
  M[sp] = -M[sp]

def And(): # Logical And
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] and M[sp]
  sp = sp - 1

def Lor(): # Logical Or
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] or M[sp]
  sp = sp - 1

def Not(): # Logical Not
  global sp, pc, Index, D, H, M
  M[sp] = not M[sp]

def Les(): # Less
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] < M[sp]
  sp = sp - 1

def Leq(): # Less or Equal
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] <= M[sp]
  sp = sp - 1

def Grt(): # Greater
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] > M[sp]
  sp = sp - 1

def Gre(): # Greater or Equal
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] >= M[sp]
  sp = sp - 1

def Equ(): # Equal
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] == M[sp]
  sp = sp - 1

def Neq(): # Not Equal
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] != M[sp]
  sp = sp - 1

def Jmp(p): # Jump
  global sp, pc, Index, D, H, M
  pc = Index[p]

def Jof(p): # Jump on False
  global sp, pc, Index, D, H, M
  if not M[sp]:
    pc = Index[p]
  sp = sp - 1

def Alc(n): # Allocate memory
  global sp, pc, Index, D, H, M
  sp = sp + n

def Dlc(n): # Deallocate memory
  global sp, pc, Index, D, H, M
  sp = sp - n

def Cfu(p): # Call Function
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = pc + 1
  pc = p-1

def Enf(k): # Enter Function
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = D[k]
  D[k] = sp + 1

def Ret(k, n): # Return from Function
  global sp, pc, Index, D, H, M
  D[k] = M[sp]
  pc = M[sp - 1] - 1
  sp = sp - (n + 2)

def Idx(k): # Index
  global sp, pc, Index, D, H, M
  M[sp - 1] = M[sp - 1] + M[sp] * k
  sp = sp - 1

def Grc(): # Get(Load) Reference Contents
  global sp, pc, Index, D, H, M
  M[sp] = M[M[sp]]

def Lmv(k): # Load multiple values
  global sp, pc, Index, D, H, M
  t = M[sp]
  M[sp:sp+k] = M[t:t+k]
  sp += (k-1)

def Smv(k): # Store multiple Values
  global sp, pc, Index, D, H, M
  t = M[sp - k]
  M[t:t+k] = M[sp-k+1:sp+1]
  sp -= (k + 1)

def Smr(k): # Store multiple References
  global sp, pc, Index, D, H, M
  t1 = M[sp - 1]
  t2 = M[sp]
  M[t1:t1+k] = M[t2:t2+k]
  sp -= 1

def Sts(k): # Store string constant on reference
  global sp, pc, Index, D, H, M
  adr = M[sp]
  M[adr] = len(H[k])
  for c in H[k]:
      adr = adr + 1
      M[adr] = c;
  sp = sp - 1

def Rdv(): # Read single Value
  global sp, pc, Index, D, H, M
  sp = sp + 1
  M[sp] = int(input().strip())

def Rds(): # Read String and store it on stack reference
  global sp, pc, Index, D, H, M
  str = input()
  adr = M[sp]
  M[adr] = len(str)
  for k in str:
      adr = adr + 1
      M[adr] = k
  sp = sp - 1

def Prv(): # Print Value
  global sp, pc, Index, D, H, M
  print(M[sp], end='')
  sp = sp - 1

def Prt(k): # Print Multiple Values
  global sp, pc, Index, D, H, M
  data = [repr(x) for x in M[sp-k+1:sp+1]]
  print(','.join(data), end='')
  sp -= k

def Prc(i): # Print String constant
  global sp, pc, Index, D, H, M
  print(H[i], end = "")

def Prs(): # Print contents of a string location
  global sp, pc, Index, D, H, M
  adr = M[sp]
  len = M[adr]
  for i in range(0, len):
     adr = adr + 1
     print(M[adr], end="")
  sp = sp - 1

def Stp(): # Start Program
  global sp, pc, Index, D, H, M
  sp = -1
  D[0] = 0

def Lbl(i): # No operation
  global sp, pc, Index, D, H, M
  pass

def End(): # Stop execution
  global sp, pc, Index, D, H, M
  pc = -1

InstructionMap = {
'ldc': Ldc, # Load constant
'ldv': Ldv, # Load value
'ldr': Ldr, # Load reference
'stv': Stv, # Store value
'lrv': Lrv, # Load reference value
'srv': Srv, # Store reference value
'add': Add, # Add
'sub': Sub, # Subtract
'mul': Mul, # Multiply
'div': Div, # Division
'mod': Mod, # Modulus
'neg': Neg, # Negate
'and': And, # Logical And
'lor': Lor, # Logical Or
'not': Not, # Logical Not
'les': Les, # Less
'leq': Leq, # Less or Equal
'grt': Grt, # Greater
'gre': Gre, # Greater or Equal
'equ': Equ, # Equal
'neq': Neq, # Not Equal
'jmp': Jmp, # Jump
'jof': Jof, # Jump on False
'alc': Alc, # Allocate memory
'dlc': Dlc, # Deallocate memory
'cfu': Cfu, # Call Function
'enf': Enf, # Enter Function
'ret': Ret, # Return from Function
'idx': Idx, # Index
'grc': Grc, # Get(Load) Reference Contents
'lmv': Lmv, # Load multiple values
'smv': Smv, # Store multiple Values
'smr': Smr, # Store multiple References
'sts': Sts, # Store string constant on reference
'rdv': Rdv, # Read single Value
'rds': Rds, # Read String and store it on stack reference
'prv': Prv, # Print Value
'prt': Prt, # Print Multiple Values
'prc': Prc, # Print String constant
'prs': Prs, # Print contents of a string location
'stp': Stp, # Start Program
'lbl': Lbl, # No operation
'end': End  # Stop execution
}

if __name__ == "__main__":
  aparser = argparse.ArgumentParser(description='LYA Virtual Machine')
  aparser.add_argument('-i', '--input', required=True, help="Input file")
  aparser.add_argument('-r', '--run', action='store_true', help='Rum LYA code from source')
  aparser.add_argument('-t', '--trace', action='store_true', help='Print trace while run program')
  args = aparser.parse_args()

  if args.run:
    H, P = lya_compile(args.input)
  else:
    lyacfile = open(args.input)
    exec(lyacfile.read())

  # construct labels
  for inst in P:
    if inst[0] == 'lbl':
      Index[inst[1]] = pc
    pc += 1

  # run code
  pc = 0
  while pc >= 0:
    inst = P[pc]
    name = inst[0]
    fargs = inst[1:]
    if args.trace:
      print(">>>> pc = %3d ( %s %s)" %
            (pc, name, "".join(map(lambda x: str(x)+" ",fargs))))
    InstructionMap[name](*fargs)
    pc = pc + 1 if pc >= 0 else -1
