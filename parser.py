################################################################################
## MC911 - Compiler Construction Laboratory - 2016.01
################################################################################
## Victor Seixas Souza (ra118896)
## Danilo Carvalho Martins (ra155123)
################################################################################
""" Lya Compiler Parser
"""

import ply.yacc as yacc
from lexer import LyaLexer
from nodetypes import *

precedence = (
  ('right','UMINUS'),
)

class MakeWithList(object):
  def __init__(self,sep=' ',sep_token=' '):
    self.sep = sep
    self.sep_token = sep_token

  def __call__(self,stmt_f):
    prod_name = stmt_f.__doc__.split(':')[0].strip()
    list_prod_name = 'p_%s_list'%prod_name
    list_prod = ('''{stmt}_list : {stmt} {sep} {stmt}_list
                                | {stmt}''').format(stmt=prod_name,
                                                    sep=self.sep_token)
    def list_wrapper(t):
      # print 'Wrapper > ' + str(list(t))
      if len(t) == 2:
        t[0] = [t[1]]
      else:
        t[0] = [t[1]] + list(t)[-1]
    list_wrapper.__name__ = list_prod_name
    list_wrapper.__doc__ = list_prod
    globals()[list_prod_name] = list_wrapper
    return stmt_f

def p_program(t):
  '''program : stmt_list'''
  t[0] = Program(t[1])

@MakeWithList()
def p_stmt(t):
  '''stmt : decl_stmt
          | syno_stmt
          | newm_stmt
          | procedure_statement
          | action_statement'''
  t[0] = t[1]

def p_decl_stmt(t):
  '''decl_stmt : DCL decl_list SEMI'''
  t[0] = DeclStmt(t[2])

@MakeWithList(',','COMMA')
def p_decl(t):
  '''decl : id_list mode init
          | id_list mode'''
  if len(t) == 3:
    t[0] = Decl(t[1],t[2])
  else:
    t[0] = Decl(t[1],t[2],init=t[3])

def p_init(t):
  '''init : ASSIGN expr'''
  t[0] = t[2]

@MakeWithList(',','COMMA')
def p_id(t):
  '''id : ID'''
  t[0] = Id(t[1])

def p_syno_stmt(t):
  '''syno_stmt : SYN syno_list SEMI'''
  t[0] = t[2]

@MakeWithList(',','COMMA')
def p_syno(t):
  '''syno : id_list ASSIGN const_expr
          | id_list mode ASSIGN const_expr'''
  if len(t) == 4:
    t[0] = Synonym(t[1],t[3])
  else:
    t[0] = Synonym(t[1],t[4],mode=t[2])

def p_const_expr(t):
  '''const_expr : expr'''
  t[0] = t[1]

def p_newm_stmt(t):
  '''newm_stmt : TYPE newm_list SEMI'''
  t[0] = t[2]

@MakeWithList(',','COMMA')
def p_newm(t):
  '''newm : id_list ASSIGN mode'''
  t[0] = ModeDefinition(t[1],t[3])

def p_mode(t):
  '''mode : id
          | discrete_mode
          | reference_mode
          | composite_mode'''
  t[0] = Mode(t[1])

def p_discrete_mode(t):
  '''discrete_mode : integer_mode
                   | boolean_mode
                   | character_mode
                   | discrete_range_mode'''
  t[0] = DiscreteMode(t[1])

def p_integer_mode(t):
  '''integer_mode : INT'''
  t[0] = t[1]

def p_bool_mode(t):
  '''boolean_mode : BOOL'''
  t[0] = t[1]

def p_character_mode(t):
  '''character_mode : CHAR'''
  t[0] = t[1]

def p_discrete_range_mode(t):
  '''discrete_range_mode : id LPAREN literal_range RPAREN
                         | discrete_mode LPAREN literal_range RPAREN '''
  t[0] = DiscreteRangeMode(t[1],t[3])

def p_literal_range(t):
  '''literal_range : lower_bound DOTS upper_bound'''
  t[0] = LiteralRange(t[1],t[3])

def p_lower_bound(t):
  '''lower_bound : expr'''
  t[0] = t[1]

def p_upper_bound(t):
  '''upper_bound : expr'''
  t[0] = t[1]

def p_reference_mode(t):
  '''reference_mode : REF mode'''
  t[0] = ReferenceMode(t[2])

def p_composite_mode(t):
  '''composite_mode : string_mode
                    | array_mode'''
  t[0] = t[1]

def p_string_mode(t):
  '''string_mode : CHARS LBRKT string_length RBRKT'''
  t[0] = StringMode(t[3])

def p_string_length(t):
  '''string_length : integer_literal'''
  t[0] = t[1]

def p_array_mode(t):
  '''array_mode : ARRAY LBRKT index_mode_list RBRKT element_mode'''
  t[0] = ArrayMode(t[3],t[5])

@MakeWithList(',','COMMA')
def p_index_mode(t):
  '''index_mode : discrete_mode
                | literal_range'''
  t[0] = t[1]


def p_element_node(t):
  '''element_mode : mode'''
  t[0] = t[1]

def p_location(t):
  '''location : location_name
              | dereferenced_reference
              | container_element
              | container_slice
              | call_action'''
  t[0] = t[1]

def p_location_name(t):
  '''location_name : id'''
  t[0] = t[1]

def p_dereferenced_reference(t):
  '''dereferenced_reference : location RARROW'''
  t[0] = DeRef(t[1])

def p_container_slice(t):
  '''container_slice : location LBRKT lower_bound DOTS upper_bound RBRKT'''
  t[0] = ContainerSlice(t[1],t[3],t[5])

def p_container_element(t):
  '''container_element : location LBRKT expr_list RBRKT'''
  t[0] = ContainerAccess(t[1],t[3])

@MakeWithList(',','COMMA')
def p_expr(t):
  '''expr : operand0
          | conditional_expression'''
  t[0] = Expression(t[1])

# This primitive value expression can contain non-literals inside parenthesized_expression
def p_primitive_value(t):
  '''primitive_value : integer_literal
                     | boolean_literal
                     | empty_literal
                     | character_literal
                     | character_string_literal
                     | parenthesized_expression
                     | value_array_element
                     | value_array_slice'''
  t[0] = t[1]

def p_parenthesized_expression(t):
  '''parenthesized_expression : LPAREN expr RPAREN'''
  t[0] = t[2]

def p_integer_literal(t):
  '''integer_literal : ICONST'''
  t[0] = Int(t[1])

def p_boolean_literal(t):
  '''boolean_literal : TRUE
                     | FALSE'''
  t[0] = Boolean(t[1])

def p_empty_literal(t):
  '''empty_literal : NULL'''
  t[0] = Empty()

def p_character_literal(t):
  '''character_literal : CCONST'''
  t[0] = Char(t[1])

def p_character_string_literal(t):
  '''character_string_literal : SCONST'''
  t[0] = String(t[1])

def p_value_array_element(t):
  '''value_array_element : primitive_value LBRKT expr_list RBRKT'''
  t[0] = AccessPrimitive(t[1],t[3])

def p_value_array_slice(t):
  '''value_array_slice : primitive_value LBRKT lower_element DOTS upper_element RBRKT'''
  t[0] = SlicePrimitive(t[1],t[3],t[5])


def p_lower_element(t):
  '''lower_element : integer_expression'''
  t[0] = t[1]

def p_upper_element(t):
  '''upper_element : integer_expression'''
  t[0] = t[1]

def p_conditional_expression(t):
  '''conditional_expression : IF boolean_expression then_expression else_expression FI
                            | IF boolean_expression then_expression elsif_expression else_expression FI'''
  # Third argument will be [] if no elsif
  if len(t) == 6:
    t[0] = Conditional(t[2],t[3],t[4])
  else:
    t[0] = Conditional(t[2],t[3],t[5],elsif=t[4])

def p_boolean_expression(t):
  '''boolean_expression : expr'''
  t[0] = BooleanExpr(t[1])

def p_then_expression(t):
  '''then_expression : THEN expr'''
  t[0] = Then(t[2])

def p_else_expression(t):
  '''else_expression : ELSE expr'''
  t[0] = Else(t[2])

# Returns a list of (cond,branch) for all of the elsifs
def p_elsif_expression(t):
  '''elsif_expression : ELSIF boolean_expression then_expression
                      | elsif_expression ELSIF boolean_expression then_expression'''
  if len(t) == 4:
    t[0] = [Elsif(t[2],t[3])]
  else:
    t[0] = t[1] + [Elsif(t[3],t[4])]

# These handle precedence
# I'm currently building a list tree, this is kind of fiddly and I might switch
# to a real tree later.
def p_operand0(t):
  '''operand0 : operand1
              | operand0 operator1 operand1'''
  t[0] = t[1] if len(t) == 2 else BinaryOperation(t[1],t[2],t[3])

def p_operator1(t):
  '''operator1 : relational_operator
               | membership_operator'''
  t[0] = Operator(t[1])

def p_relational_operator(t):
  '''relational_operator : AND
                         | OR
                         | NOT
                         | EQ
                         | NEQ
                         | GT
                         | GEQ
                         | LT
                         | LEQ'''
  t[0] = t[1]

def p_membership_operator(t):
  '''membership_operator : IN'''
  t[0] = t[1]

def p_operand1(t):
  '''operand1 : operand2
              | operand1 operator2 operand2'''
  t[0] = t[1] if len(t) == 2 else BinaryOperation(t[1],t[2],t[3])

def p_operator2(t):
  '''operator2 : arithmetic_additive_operator
               | string_concatenation_operator'''
  t[0] = Operator(t[1])

def p_arithmetic_additive_operator(t):
  '''arithmetic_additive_operator : PLUS
                                  | MINUS'''
  t[0] = t[1]

def p_arithmetic_multiplicative_operator(t):
  '''arithmetic_multiplicative_operator : TIMES
                                        | DIVIDE
                                        | REST'''
  t[0] = Operator(t[1])

def p_string_concatenation_operator(t):
  '''string_concatenation_operator : CAT'''
  t[0] = t[1]

def p_operand2(t):
  '''operand2 : operand3
              | operand2 arithmetic_multiplicative_operator operand3'''
  t[0] = t[1] if len(t) == 2 else BinaryOperation(t[1],t[2],t[3])

def p_operand3(t):
  '''operand3 : monadic_operator operand4 %prec UMINUS
              | operand4'''
  t[0] = t[1] if len(t) == 2 else MonadicOperator(t[1],t[2])

def p_monadic_operator(t):
  '''monadic_operator : MINUS
                      | NOT'''
  t[0] = t[1]

def p_operand4(t):
  '''operand4 : location
              | referenced_location
              | primitive_value'''
  t[0] = t[1]

def p_referenced_location(t):
  '''referenced_location : RARROW location'''
  t[0] = Location(t[2])

@MakeWithList()
def p_action_statement(t):
  '''action_statement : action SEMI
                      | label_id DOTS action SEMI'''
  if len(t) == 3:
    t[0] = Action(t[1])
  else:
    t[0] = Action(t[3],label_id=t[1])

def p_label_id(t):
  '''label_id : id'''
  t[0] = t[1]

def p_action(t):
  '''action : bracketed_action
            | assignment_action
            | call_action
            | exit_action
            | return_action
            | result_action
            | raw_action '''
  t[0] = TopLevelAction(t[1])

def p_raw_action(t):
  '''raw_action : RAWCMD'''
  t[0] = Raw(t[1])

def p_bracketed_action(t):
  '''bracketed_action : if_action
                      | do_action'''
  t[0] = t[1]

def p_assignment_action(t):
  '''assignment_action : location assigning_operator expr'''
  t[0] = Assignment(*t[1:])

# Just return these together and handle resposability upstream
def p_assigning_operator(t):
  '''assigning_operator : closed_dyadic_operator ASSIGN
                        | ASSIGN'''
  if len(t) == 2:
    t[0] = AssignmentOperator(t[1])
  else:
    t[0] = AssignmentOperator(''.join((t[1].operator,t[2])))

def p_closed_dyadic_operator(t):
  '''closed_dyadic_operator : arithmetic_additive_operator
                            | arithmetic_multiplicative_operator
                            | string_concatenation_operator'''
  t[0] = Operator(t[1])

def p_if_action(t):
  '''if_action : IF boolean_expression then_clause else_clause FI
               | IF boolean_expression then_clause FI'''
  if len(t) == 5:
    t[0] = IfAction(t[2],t[3])
  else:
    t[0] = IfAction(t[2],t[3],else_clause=t[4])

def p_then_clause(t):
  '''then_clause : THEN action_statement_list'''
  t[0] = t[2]

def p_else_clause(t):
  '''else_clause : ELSE action_statement_list
                 | ELSIF boolean_expression then_clause
                 | ELSIF boolean_expression then_clause else_clause'''
  if len(t) == 3:
    t[0] = [ElseClause(t[2])]
  elif len(t) == 4:
    t[0] = [ElsifClause(t[2],t[3])]
  else:
    t[0] = [ElsifClause(t[2],t[3])] + t[4]
  t[0][0]._lineno = t.lineno(1)

def p_do_action(t):
  '''do_action : DO action_statement_list OD
               | DO control_part SEMI action_statement_list OD'''
  if len(t) == 4:
    t[0] = Do(t[2])
  else:
    t[0] = Do(t[4],control=t[2])

def p_control_part(t):
  '''control_part : for_control
                  | while_control
                  | for_control while_control'''
  t[0] = list(t)[1:]

def p_for_control(t):
  '''for_control : FOR iteration'''
  t[0] = For(t[2])

def p_iteration(t):
  '''iteration : step_enumeration
               | range_enumeration'''
  t[0] = t[1]

def p_step_enumeration(t):
  '''step_enumeration : loop_counter ASSIGN start_value end_value
                      | loop_counter ASSIGN start_value step_value end_value
                      | loop_counter ASSIGN start_value step_value DOWN end_value
                      | loop_counter ASSIGN start_value DOWN end_value'''
  #t[0] = STEP_ENUM(t[1:])
  if len(t) == 5:
    t[0] = Step(t[1],t[3],t[4])
  elif len(t) == 7:
    t[0] = Step(t[1],t[3],t[6],step=t[4],down=True)
  elif hasattr(t[4], 'lower') and t[4].lower() == 'down':
    t[0] = Step(t[1],t[3],t[5],down=True)
  else:
    t[0] = Step(t[1],t[3],t[5],step=t[4])

def p_loop_counter(t):
  '''loop_counter : id'''
  t[0] = t[1]

def p_start_value(t):
  '''start_value : discrete_expression'''
  t[0] = t[1]

def p_step_value(t):
  '''step_value : BY integer_expression'''
  t[0] = t[2]

def p_end_value(t):
  '''end_value : TO discrete_expression'''
  t[0] = t[2]

def p_discrete_expression(t):
  '''discrete_expression : expr'''
  t[0] = t[1]

def p_range_enumeration(t):
  '''range_enumeration : loop_counter IN discrete_mode
                       | loop_counter DOWN IN discrete_mode'''
  if len(t) == 4:
    t[0] = Range(t[1],t[3],'up')
  else:
    t[0] = Range(t[1],t[4],'down')

def p_while_control(t):
  '''while_control : WHILE boolean_expression'''
  t[0] = While(t[2])

def p_call_action(t):
  '''call_action : procedure_call
                 | builtin_call'''
  t[0] = t[1]

@MakeWithList(',','COMMA')
def p_parameter(t):
  '''parameter : expr'''
  t[0] = t[1]

def p_exit_action(t):
  '''exit_action : EXIT label_id'''
  t[0] = Exit(t[2])

def p_return_action(t):
  '''return_action : RETURN result
                   | RETURN'''
  t[0] = Return(t[2] if len(t) == 3 else None)

def p_result_action(t):
  '''result_action : RESULT result'''
  t[0] = Result(t[2])

def p_result(t):
  '''result : expr'''
  t[0] = t[1]

def p_procedure_call(t):
  '''procedure_call : id  LPAREN RPAREN
                    | id  LPAREN parameter_list RPAREN'''
  if len(t) == 4:
    t[0] = ProcedureCall(t[1])
  else:
    t[0] = ProcedureCall(t[1],fargs=t[3])

def p_builtin_name(t):
  '''builtin_name : NUM
                  | PRED
                  | SUCC
                  | UPPER
                  | LOWER
                  | LENGTH
                  | READ
                  | LSC
                  | GSS
                  | PTS
                  | SFS
                  | PRINT'''
  t[0] = BuiltinName(t[1])

def p_procedure_statement(t):
  '''procedure_statement : label_id DOTS procedure_definition SEMI'''
  t[0] = ProcedureStatement(t[1],t[3])

def p_procedure_definition(t):
  '''procedure_definition : PROC LPAREN RPAREN SEMI stmt_list END
                          | PROC LPAREN RPAREN result_spec SEMI stmt_list END
                          | PROC LPAREN formal_parameter_list RPAREN SEMI stmt_list END
                          | PROC LPAREN formal_parameter_list RPAREN result_spec SEMI stmt_list END'''
  if len(t) == 7:
    t[0] = ProcedureDefinition(t[5])
  elif len(t) == 8:
    if t[3] == ')':
      t[0] = ProcedureDefinition(t[6],result_spec=t[4])
    else:
      t[0] = ProcedureDefinition(t[6],params=t[3])
  else:
    t[0] = ProcedureDefinition(t[7],params=t[3],result_spec=t[5])

@MakeWithList(',','COMMA')
def p_formal_parameter(t):
  '''formal_parameter : id_list parameter_spec'''
  t[0] = FormalParameter(t[1],t[2])

def p_parameter_spec(t):
  '''parameter_spec : mode
                    | mode parameter_attribute'''
  if len(t) == 2:
    t[0] = t[1]
  else:
    t[0] = LocMode(t[1])

def p_parameter_attribute(t):
  '''parameter_attribute : LOC'''
  t[0] = t[1]

def p_result_spec(t):
  '''result_spec : RETURNS LPAREN mode RPAREN
                 | RETURNS LPAREN mode result_attribute RPAREN'''
  if len(t) == 5:
    t[0] = t[3]
  else:
    t[0] = LocMode(t[3])

def p_result_attribute(t):
  '''result_attribute : LOC'''
  t[0] = t[1]

def p_integer_expression(t):
  '''integer_expression : expr'''
  t[0] = IntegerExpression(t[1])

def p_builtin_call(t):
  '''builtin_call : builtin_name LPAREN RPAREN
                  | builtin_name LPAREN parameter_list RPAREN'''
  if len(t) == 4:
    t[0] = BuiltinCall(t[1])
  else:
    t[0] = BuiltinCall(t[1],fargs=t[3])

def p_error(t):
  print('Sad: ')
  pprint(t)

# THE HACK
def hack():
  goebbels = globals().copy()
  for fun, body in goebbels.items():
    if fun[0:2] != 'p_':
      continue
    def foo(fun, body):
      def newf(t):
        body(t)
        if isinstance(t[0], AST):
          t[0]._lineno = t.lineno(1)
      newf.__doc__ = body.__doc__
      newf.__name__ = fun
      return newf
    globals()[fun] = foo(fun, body)
hack()

tokens = LyaLexer.tokens
lyalex = LyaLexer()
lyalex.build()

def ParseString(data):
    parser = yacc.yacc(start='program')
    return parser.parse(data, lexer=lyalex, tracking=True)

def RunParser(filename):
    parser = yacc.yacc(start='program', outputdir="optifiles", optimize=1)
    with open(filename) as datafile:
      return parser.parse(datafile.read(), lexer=lyalex, tracking=True)

if __name__ == '__main__':
  if len(sys.argv) == 1:
    print('python parser.py <filename>')
  else:
    for f in get_files_in_args(sys.argv[1:]):
      PrintAst(RunParser(f))
