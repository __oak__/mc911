import parser
from nodetypes import *

class SymbolTable(object):
    '''
    Class representing a symbol table.
    The default value is an empty list, as it can behave efficiently like a
    stack.
    '''
    def __init__(self):
        self.data = defaultdict(UndefinedType)

    def lookup(self, name):
        if name not in self.data:
            error("Name %s not defined"%name)
        return self.data[name]

    def __in__(self, name):
        return name in self.data

    def __getitem__(self, key):
        return self.data[key]

    def __setitem__(self, key, value):
        self.data[key] = value

    def copy(self):
        r = SymbolTable()
        r.data = self.data.copy()
        return r

    def add_list(self, id_list, idtype):
        for _id in id_list:
            self.data[_id.name] = idtype

class TypeWalker(Walker):
    def method(self, node, parent):
        if hasattr(node, '_fields_types'):
            fields = node._order if hasattr(node, '_order') else node._fields
            for field,ftype in zip(fields,node._field_types):
                ensure(field, class_from_name('%sType'%ftype))
        node._check()
        # If the subtree is constant non-primitive, prune it.
        if hasattr(node, '_replacement') and not isinstance(node, Primitive):
            for field in parent.__dict__:
                if getattr(parent, field) == node:
                    setattr(parent, field, node._replacement)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('python %s (<filename>)+'%sys.argv[0])
        sys.exit(1)

    files_to_check = get_files_in_args(sys.argv[1:])
    for fname in files_to_check:
        print('>>>> Running for %s'%fname)
        ast = parser.RunParser(fname)
        ast._closure = SymbolTable()
        walker = TypeWalker()
        walker.walk_children(ast)
        parser.PrintAst(ast)
        print('Check Ok!')

