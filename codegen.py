from syntax_analysis import *
from pprint import pprint, pformat
from copy import copy,deepcopy
import argparse

class Code(object):
    def __init__(self):
        # Structures to hold for the program
        self.code = []
        self.labels = {}
        self.functions = {}
        self.variables = {}
        self.strings = []

        # Offsets for variable positioning and labeling. This is kept in a
        # dictionary so the children can reference the same thing as the parent
        # after a shallow copy
        self.locals = {
                'var_offset': 0,
                # Starts with -2, as `d` is a nonargument
                'arg_offset': 0,
        }

        self.globals = {
                'label': 0,
                'function': 1,
        }

    def __iadd__(self, more_code):
        '''Basic operation of composing code'''
        if isinstance(more_code, list):
            for code_chunk in more_code:
                self += code_chunk
        elif isinstance(more_code, tuple):
            # Handle variables
            if isinstance(more_code[1], tuple):
                self.code.append((more_code[0],
                                  more_code[1][0],
                                  more_code[1][1]))
            else:
                self.code.append(more_code)
        elif isinstance(more_code, str):
            self.code.append((more_code,))
        elif isinstance(more_code, Code):
            self.code += more_code.code
        else:
            import traceback
            traceback.print_stack()
            error('Unexpected argument')
        return self

    def add_string(self, name):
        self.strings.append(name)
        return len(self.strings)-1

    def variable(self, name):
        '''Get the offset and the context of a variable'''
        return self.variables[name]

    def make_variable(self, var_name, var_type):
        '''Create a variable in the current context'''
        self.variables[var_name] = (self.globals['function']-1,
                                    self.locals['var_offset'])
        self.locals['var_offset'] += var_type.synonym.size
        return self.variables[var_name]

    def add_argument(self, var_name, var_size):
        '''Add a variable with the right negative offset'''
        self.variables[var_name] = (self.globals['function']-1,
                                    -3 - self.locals['arg_offset'])
        self.locals['arg_offset'] += var_size
        return self.variables[var_name]

    def function(self, fname):
        '''Get the id of a function'''
        return self.functions[fname]

    def make_function(self, fname):
        '''Reserve a function id for a function.'''
        self.functions[fname] = self.globals['function']
        return self.function(fname)

    def begin_function(self, fname):
        self.globals['function'] += 1
        self.locals['var_offset'] = 0
        self.locals['arg_offset'] = 0

    def loc_var_mem(self):
        '''Returns the ammount allocated for the function.'''
        return self.locals['var_offset']

    def argsize(self):
        '''Returns the size that the arguments take'''
        return self.locals['arg_offset'] - 1

    def label(self, name):
        '''Return the label id of a label identified by name.'''
        return self.labels[name]

    def make_label(self, name=''):
        '''Reserve a label that might or might not be named'''
        self.labels[name] = self.globals['label']
        self.globals['label'] += 1
        return self.label(name)

    def copy(self):
        new_code = copy(self)
        new_code.code = []
        return new_code

    def deepcopy(self):
        # Create new bindings for everything
        new_code = deepcopy(self)
        new_code.code = []
        new_code.globals = self.globals
        return new_code

    # Remove trivial reundancies
    def optimize(self):
        for i in range(1,len(self.code)):
            # Remove stupid variable acces
            if self.code[i] == ('lmv', 1) and self.code[i-1][0] == 'ldr':
                vals = list(self.code[i-1])
                vals[0] = 'ldv'
                self.code[i-1] = tuple(vals)
                self.code[i] = (None,)
            elif self.code[i] == ('smv', 1) and self.code[i-1][0] == 'ldr':
                vals = list(self.code[i-1])
                vals[0] = 'stv'
                self.code[i-1] = tuple(vals)
                self.code[i] = (None,)
            # Remove zero allocation
            elif self.code[i] == ('alc', 0) or self.code[i] == ('dlc', 0):
                self.code[i] = (None,)
        self.code = list(filter(lambda x: x != (None,), self.code))

    def resolve_functions(self):
        # resolve fnmark offsets
        mark_to_offset= {}
        offset = 0
        for instr in self.code:
            if instr[0] == 'fnmark':
                mark_to_offset[instr[1]] = offset
            else:
                offset += 1
        result = []
        # Remove the fnmarks and resolve offsets
        for instr in self.code:
            if instr[0] == 'cfu':
                result.append(('cfu', mark_to_offset[instr[1]]))
            elif instr[0] != 'fnmark':
                result.append(instr)
        self.code = result

def lya_compile(sourcefile):
    ast = parser.RunParser(sourcefile)
    ast._closure = SymbolTable()
    walker = TypeWalker()
    walker.walk_children(ast)
    ast._code = Code()
    ast()
    ast._code.optimize()
    ast._code.resolve_functions()
    return (ast._code.strings, ast._code.code)

if __name__ == '__main__':
    aparser = argparse.ArgumentParser(description='LYA Compiler')
    aparser.add_argument('-i', '--input', required=True, help="Input LYA source file")
    aparser.add_argument('-o', '--output', required=True, help="Output LYA object file")
    args = aparser.parse_args()
    H,P = lya_compile(args.input)
    outfile = open(args.output,'w')
    outfile.write('H={}\nP={}'.format(H,P))
