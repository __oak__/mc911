################################################################################
## MC911 - Compiler Construction Laboratory - 2016.01
################################################################################
## Victor Seixas Souza (ra118896)
## Danilo Carvalho Martins (ra155123)
################################################################################
""" Lya Compiler Lexer
"""

import ply.lex as lex
import os

class LyaLexer(object):
  """ A lexer for the Lya language. After build() it, set the
        input text with input(), and call token() to get new
        tokens.
  """

  def __init__(self):
    """ Create a new Lexer.
    """
    # Last token returned from self.token()
    self.last_token = None

  def build(self):
    """ Builds the lexer from the specification. Must be
          called after the lexer object is created.
    """
    self.lexer = lex.lex(object=self, outputdir="optifiles", optimize=1)

  #def reset_lineno(self):
  #  """ Resets the internal line number counter of the lexer.
  #  """
  #  self.lexer.lineno = 1

  def input(self, text):
    self.lexer.input(text)

  def token(self):
    self.last_token = self.lexer.token()
    return self.last_token

  #def find_tok_column(self, token):
  #  """ Find the column of the token in its line.
  #  """
  #  last_cr = self.lexer.lexdata.rfind('\n', 0, token.lexpos)
  #  return token.lexpos - last_cr

  def test_suite(self):
    """ Run all test cases on testcases/ folder.
          Outputs on resuts/ folder.
    """
    try:
      os.mkdir('results')
    except OSError:
      pass
    for f in sorted(os.listdir('testcases')):
      produced_tokens = []
      with open('testcases/'+f) as datafile:
        data = datafile.read();
        self.lexer.input(data)
        self.lexer.lineno = 0
        for tok in self.lexer:
          produced_tokens.append(tok)
          open('results/'+f,'w').write('\n'.join(map(repr,produced_tokens)))

  #### Private ####

  t_ignore = ' \t'

  reserved_words = {
    'array'   :'ARRAY',
    'by'      :'BY',
    'chars'   :'CHARS',
    'dcl'     :'DCL',
    'do'      :'DO',
    'down'    :'DOWN',
    'else'    :'ELSE',
    'elsif'   :'ELSIF',
    'end'     :'END',
    'exit'    :'EXIT',
    'fi'      :'FI',
    'for'     :'FOR',
    'if'      :'IF',
    'in'      :'IN',
    'loc'     :'LOC',
    'type'    :'TYPE',
    'od'      :'OD',
    'proc'    :'PROC',
    'ref'     :'REF',
    'result'  :'RESULT',
    'return'  :'RETURN',
    'returns' :'RETURNS',
    'syn'     :'SYN',
    'then'    :'THEN',
    'to'      :'TO',
    'while'   :'WHILE'
    }

  predefined_words = {
    'bool'  :'BOOL',
    'char'  :'CHAR',
    'false' :'FALSE',
    'int'   :'INT',
    'length':'LENGTH',
    'lower' :'LOWER',
    'load_string_contents' :'LSC',
    'null'  :'NULL',
    'num'   :'NUM',
    'pred'  :'PRED',
    'print' :'PRINT',
    'push_to_stack' :'PTS',
    'read'  :'READ',
    'succ'  :'SUCC',
    'get_string_size' :'GSS',
    'store_from_stack' :'SFS',
    'true'  :'TRUE',
    'upper' :'UPPER'
    }

  keywords = {}
  keywords.update(reserved_words)
  keywords.update(predefined_words)

  tokens = [
    'PLUS','MINUS','TIMES','DIVIDE','REST', # Arithmetical
    'ASSIGN','SEMI','COMMA','DOTS',         # Control
    'LPAREN','RPAREN','LBRKT','RBRKT',      # Delimiters
    'AND','OR','NOT',                       # Logical
    'EQ','NEQ','GT','GEQ','LT','LEQ',       # Relational
    'CCONST','SCONST','ICONST','ID',        # Constants and Names
    'RARROW',                               # Reference
    'CAT',                                  # String
    'RAWCMD'                                # Token used for raw command parsing
    ] + list(keywords.values())

    # Arithmetical
  t_PLUS    = r'\+'
  t_MINUS   = r'-'
  t_TIMES   = r'\*'
  t_DIVIDE  = r'/'
  t_REST    = r'%'

  # Control
  t_ASSIGN  = r'='
  t_SEMI    = r';'
  t_COMMA   = r','
  t_DOTS    = r':'

  # Deilimers
  t_LPAREN  = r'\('
  t_RPAREN  = r'\)'
  t_LBRKT   = r'\['
  t_RBRKT   = r'\]'

  # Logical
  t_AND   = r'&&'
  t_OR    = r'\|\|'
  t_NOT   = r'!'

  # Relational
  t_EQ    = r'=='
  t_NEQ   = r'!='
  t_GT    = r'>'
  t_GEQ   = r'>='
  t_LT    = r'<'
  t_LEQ   = r'<='

  # Constants and Names
  def t_CCONST(self,t):
    r"('.')|(\^\([0-9]+\))"
    if len(t.value) == 3:
      t.value = t.value[1]
    elif len(t.value) > 3:
      t.value = chr(int(t.value[2:-1]))
    return t

  t_SCONST = r'"(?:\\.|[^"\\])*"'

  t_RAWCMD = r'\?(?:\\.|[^\?\\])*\?'

  def t_SCONST(self,t):
    r'"(?:\\.|[^"\\])*"'
    t.value = bytearray(t.value,'ascii').decode('unicode-escape')
    return t

  def t_ICONST(self,t):
    r'\d+'
    t.value = int(t.value)
    return t

  def t_ID(self,t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = self.keywords.get(t.value,'ID')
    return t

  # Reference
  t_RARROW  = r'->'
  # String
  t_CAT   = r'&'

  # Line Number
  def t_newline(self,t):
    r'\n+'
    t.lexer.lineno += len(t.value)

  # Comments
  def t_LCOMMENT(self,t):
    r'//.*'
    t.lexer.lineno += 1

  def t_BCOMMENT(self,t):
    r'/\*(.|\n)*?\*/'
    t.lexer.lineno += t.value.count('\n')

  # Error handling rules
  def t_unterminatedString(self,t):
    r'"""(.|\n)*?'
    print("lineno %d : Unterminated string" % t.lexer.lineno)
    t.lexer.skip(1)

  def t_unterminatedString2(self,t):
    r"'''(.|\n)*?"
    print("lineno %d : Unterminated string" % t.lexer.lineno)
    t.lexer.skip(1)

  def t_unterminatedComment(self,t):
    r'/\*(.|\n)*?'
    print("lineno %d : Unterminated comment" % t.lexer.lineno)
    t.lexer.skip(1)

  def t_error(self,t):
    print("Bad string escape code '\..'")
    t.lexer.skip(1)

  # Build the lexer
  #def build(self,**kwargs):
  #  self.lexer = lex.lex(module=self, **kwargs)

################################################################################
## Some Test Code

if __name__ == '__main__':
  #Test the Lexer
  lyalex = LyaLexer()
  lyalex.build()
  lyalex.test_suite()
