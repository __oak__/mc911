from utils import *

class AnyType(object):
    ''' Base for all the Type classes.

    Each type has a dictionary with the operations a type can be subject to and
    what type is returned after the operation `String * Char -> String` (ex:
    'a'*3 = 'aaa').

    The operations are defined in the OPERATIONS list and get dynamically added
    to those classes later, operators under _trivial_ are added as
    `type OP type = type.`
    '''
    trivial = []
    monadic = []
    size = 1

class IntType(AnyType):
    trivial = ['+', '-', '*', '/', '%']
    monadic = ['-']

class LocType(AnyType):
    pass

# This is only a reference with size 1
class StringType(AnyType):
    trivial = ['&']

class CharType(AnyType):
    pass

class BooleanType(AnyType):
    trivial = ['&&', '||', '==', '!=']
    monadic = ['!']

class NoneType(AnyType):
  pass

class ArrayType(AnyType):
    referenced = NoneType

class RefType(AnyType):
    monadic = ['->']
    referenced = NoneType

class UndefinedType(AnyType):
    pass

class NoneType(AnyType):
    size = 0
    pass

class ModeType(AnyType):
    synonym = UndefinedType;

class FuncType(AnyType):
    arguments = []

class LabelType(AnyType):
    pass

def type_name(c):
    return c.__name__.lower().split('type')[0]

def class_from_name(name):
    if name == 'bool':
        return BooleanType
    else:
        return getattr(sys.modules[__name__], name.capitalize()+'Type')

OPERATIONS = [
        'any in array => bool',
        'char in string => bool',
        'int == int => bool',
        'int != int => bool',
        'int >  int => bool',
        'int >= int => bool',
        'int <  int => bool',
        'int <= int => bool',
        'char == char => bool',
        'char != char => bool',
        'char >  char => bool',
        'char >= char => bool',
        'char <  char => bool',
        'char <= char => bool',
        'string == string => bool',
        'string != string => bool'
]


NAME_TO_TYPES = {type_name(c):c
                 for c
                 in list(globals().values())
                 if isclass(c) and issubclass(c, AnyType)}

# Assign operations
def assign_operators():
    for typename, typeclass in NAME_TO_TYPES.items():
        typeclass.bops = defaultdict(dict)
        typeclass.mops = defaultdict(dict)
        for operator in typeclass.trivial:
            typeclass.bops[operator][class_from_name(typename)] = typeclass

        for operator in typeclass.monadic:
            typeclass.mops[operator] = typeclass
    for operation in OPERATIONS:
        op1, op, op2, _, res = operation.split()
        NAME_TO_TYPES[op1].bops[op][class_from_name(op2)] = class_from_name(res)
        NAME_TO_TYPES[op2].bops[op][class_from_name(op1)] = class_from_name(res)

assign_operators()

def error(errmsg):
    print('Error: %s'%errmsg);
    sys.exit(1)

def ensure(obj,
           types,
           errmsg='Tried to use type {used}, can only use {allowed}'):
    '''Ensure that obj or it's elements, if a list, are of type t.'''
    types = types if isinstance(types, list) else [types]
    obj = obj if isinstance(obj, list) else [obj]
    for expr in obj:
        if not any(issubclass(expr._type, t) or issubclass(t, expr._type)
                   for t
                   in types):
            embed()
            error(errmsg.format(used=expr._type,allowed=types))

class TypeFactory(object):
    types = {}
    def loc(underlying):
        argnames = 'Loc%sType'%repr(underlying)
        return type(argnames,
                    (underlying, LocType, TypeFactory.ref(underlying)),
                    {'underlying': underlying,
                     'size': 1})

    def function(arguments=[],rettype=NoneType):
        argnames = 'Func%sRet%sType'%(''.join([type_name(arg._type)
                                               for arg
                                               in arguments]),
                                      '' if rettype == NoneType else rettype)
        return type(argnames,
                    (FuncType,),
                    {'arguments': arguments, 'returns': rettype})

    def ref(referenced):
        argnames = 'Ref%sType'%repr(referenced)
        if argnames not in TypeFactory.types:
            TypeFactory.types[argnames] = type(argnames,
                                               (RefType,),
                                               {'referenced': referenced,
                                                'size': 1})
        return TypeFactory.types[argnames]

    def array(contains, dims):
        sizes = [up - low + 1 for (low, up) in dims]
        if any(x < 0 for x in sizes):
            print(sizes)
            error('Upper bound must be bigger than lower size')
        tot_size = reduce(lambda x,y: x*y, sizes, 1)
        argnames = 'Array%s%sType'%(repr(contains), repr(sizes))
        if argnames not in TypeFactory.types:
            TypeFactory.types[argnames] = type(argnames,
                                               (ArrayType,),
                                               {'referenced': contains,
                                                'sizes': sizes,
                                                'size': tot_size,
                                                'dims': dims})
        return TypeFactory.types[argnames]

    def string(size):
        argnames = 'String%dType'%size
        if argnames not in TypeFactory.types:
            TypeFactory.types[argnames]= type(argnames,
                                              (StringType,),
                                              # account for length
                                              {'size': size + 1,
                                               'dims': [(0,size-1)],
                                               'referenced': CharType})
        return TypeFactory.types[argnames]

    def mode(synonym):
        argnames = 'Mode%sType'%repr(synonym)
        if argnames not in TypeFactory.types:
            TypeFactory.types[argnames]= type(argnames,
                                              (ModeType,),
                                              {'synonym': synonym})
        return TypeFactory.types[argnames]
