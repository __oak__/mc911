from inspect import isclass
from lyatypes import *
import parser

# Conversion between simple lya types to nodes
def simple_lya_to_node(lyatype):
    return globals()[lyatype.__name__.split("Type")[0]]

def flatten(listoflists):
    return reduce(lambda x,y: x+y, listoflists, [])

# This function handles deep nested lists
def generate(node):
    if isinstance(node, list):
        return [generate(n) for n in node]
    else:
        return node()

def PrintAst(t):
    if isinstance(t, AST):
        PrintAstDepth(t,0)
    print()

def PrintAstDepth(t, depth):
    space = " "*(2*depth)
    if isinstance(t, AST):
        print("")
        #TODO: Add the dynamic code generation flag here.
        if hasattr(t, '_lineno'):
                print(space + str(t._lineno) + " : " + type(t).__name__,end=" ")
        else:
                print(space + " D: " + type(t).__name__,end=" ")
        if hasattr(t,'_order'):
            for c in [getattr(t,f) for f in t._order if hasattr(t,f)]:
                PrintAstDepth(c, depth + 1)
        else:
            for c in [getattr(t,f) for f in t._fields]:
                PrintAstDepth(c, depth + 1)
            for k,v in t.__dict__.items():
                if k not in t._fields and k[0] != '_':
                    PrintAstDepth(v,depth+1)
    elif isinstance(t, list):
        for c in t:
            PrintAstDepth(c, depth+1)
    else:
        print("("+str(t)+")",end="")

def tree_and_code(node):
    PrintAst(node)
    pprint('>'*10)
    pprint(node._code.code)

def extract_location(node):
    instrs = node
    instrs.code.pop()
    return instrs

class AST(object):
    """
    Base class example for the AST nodes.    Each node is expected to
    define the _fields attribute which lists the names of stored
    attributes.     The __init__() method below takes positional
    arguments and assigns them to the appropriate fields.    Any
    additional arguments specified as keywords are also assigned.

    expression-like nodes that are expect to return some kind of value do so
    through the _value field.
    """
    _fields = []
    _before = 'all'
    _after = 'none'
    def __init__(self, *args, **kwargs):
        self._const = False
        assert len(args) == len(self._fields)
        for name,value in zip(self._fields,args):
            setattr(self,name,value)
        # Assign additional keyword arguments if supplied
        for name,value in kwargs.items():
            setattr(self,name,value)

    def __repr__(self):
        PrintAst(self)
        return '' 

    # Default check behavior is to do nothing
    def _check(self):
        self._type = NoneType

    # Default behavior is to just call children
    def __call__(self):
        self._code += [generate(getattr(self, field))
                       for field
                       in self._fields]

def replace_id(tree, name, subtree, parent=AST()):
    '''Replaces every id with name name on tree by subtree'''
    if isinstance(tree, list):
        [replace_id(t, name, subtree, tree) for t in tree]
    elif isinstance(tree, AST):
        if hasattr(tree, 'name') and tree.name == name:
            for field in parent.__dict__:
                if getattr(parent, field) == tree:
                    setattr(parent, field, subtree)
        for field_name, field in tree.__dict__.items():
            if field_name[0] != '_':
                replace_id(field, name, subtree, tree)

# generic class to represent types that have all integer fields
class IntegerFields(AST):
    def _check(self):
        for cname, child in self.__dict__.items():
            if cname[0] is not '_':
                ensure(child, IntType)

#--------------------------Start of useful classes-----------------------------
class Program(AST):
    _fields = ['statements']
    def __call__(self):
        '''
        There's some code here because Program also behaves as a top level
        function
        '''
        self._code += ('stp')
        code = generate(self.statements)
        #code = [generate(statement) for statement in self.statements]
        # Alocate space for the variabels
        self._code += ('alc', self._code.loc_var_mem())
        self._code += code
        self._code += ('dlc', self._code.loc_var_mem())
        self._code += ('end')

class Raw(AST):
    _fields = ['data']
    def _check(self):
        self._type = NoneType

    def __call__(self):
        # strip the ?
        self._code += eval(self.data[1:-1])

# class with one fields that just shoots the type up
class Passthrough(AST):
    def _check(self):
        child = getattr(self, self._fields[0])
        self._type = getattr(self, self._fields[0])._type
        self._const = child._const
        if child._const:
            self._replacement = child

class Id(AST):
    _before = 'none'
    _after = 'none'
    _fields = ['name']

    # TODO: synonyms are constexprs
    def _check(self):
        #This may be either a var or a replacement node
        var = self._closure.lookup(self.name)
        # Then it's a synonym
        if isinstance(var, AST):
            self._replacement = var
        elif issubclass(var, AnyType):
            self._type = var

    def __call__(self):
        level, offset = self._code.variable(self.name)
        self._code += ('ldr', level, offset)
        self._code += ('lmv', self._type.size)
        if issubclass(self._type, LocType):
            self._code += ('lmv', self._type.size)

class Primitive(AST):
    _before = 'none'
    _after = 'none'
    _fields = ['data']
    def __init__(self, data):
        super().__init__(data)
        self._const = True
        self._type = class_from_name(type(self).__name__)

    def _check(self):
        pass

class Int(Primitive):
    def __call__(self):
        self._code += ('ldc', self.data)

class Char(Primitive):
    def __call__(self):
        self._code += ('ldc', self.data)

class String(Primitive):
    def __call__(self):
        stroff = self._code.add_string(self.data[1:-1])
        self._type = TypeFactory.string(len(self.data)-2)
        self._code = ('sts', stroff)
        # Don't know if I will have to use this
        self._value = stroff

class Boolean(Primitive):
    def __call__(self):
        self._code += ('ldc', 1 if self.data == 'true' else 0)

class Empty(Primitive):
    _fields = []

class Expression(AST):
    _fields = ['op_tree']

    def _check(self):
        if self.op_tree._const:
            self._const = True
            self._replacement = self.op_tree
        self._type = self.op_tree._type

class BooleanExpr(Expression):
    _fields, _field_types = zip(*[
        ('op_tree', 'Bool')
    ])

class Do(AST):
    _fields = ['body']
    _order = ['control','body']
    _after = 'all'
    _before = 'none'
    def _check(self):
        self._closure = self._closure.copy()
        self._type = NoneType

    def __call__(self):
        """
        Do assumes that .control returns a boolean that states wether to
        continue the loop or not.
        """
        exit = self._code.make_label('end')
        continue_label = self._code.make_label('continue')
        # Nobody else handles the continue label
        if not any(isinstance(ctrl, For) for ctrl in self.control):
            self._code += ('lbl', continue_label)
        self._code += generate(self.control)
        self._code += ('jof', exit)
        self._code += generate(self.body)
        self._code += [
                ('jmp', continue_label),
                ('lbl', exit)
        ]

class For(AST):
    _fields = ['enumerator']

class Step(AST):
    _fields = ['counter','start_value','end_value']
    _before = ['start_value','end_value']

    def _check(self):
        ensure([self.start_value, self.end_value], IntType)
        if hasattr(self, 'step'):
            ensure(self.step, IntType)
        self._closure[self.counter.name] = IntType

    def __call__(self):
        # Init code
        ctr = self._code.variable(self.counter.name)
        step = self.step() if hasattr(self, 'step') else ('ldc', 1)
        step_op = ('sub') if hasattr(self, 'down') else ('add')
        cmp_op = ('gre') if hasattr(self, 'down') else ('leq')
        no_add = self._code.make_label()
        # continue loop label
        self._code += [
                self.start_value(),
                ('stv', ctr),
                ('jmp', no_add),
                ('lbl', self._code.label('continue')),
                # ctr++
                ('ldv', ctr),
                step,
                step_op,
                ('stv', ctr),
                ('lbl', no_add),
                ('ldv', ctr),
                self.end_value(),
                # ctr < end_value 
                cmp_op
        ]

class Mode(AST):
    _fields = ['mode']
    def _check(self):
        if isinstance(self.mode, str):
            self._type = self._closure.lookup(self.mode)
        elif issubclass(self.mode._type, ModeType):
            self._type = self.mode._type
            return
        else:
            self._type = self.mode._type
        self._type = TypeFactory.mode(self._type)

class While(AST):
    _fields = ['stop_condition']


class Decl(AST):
    _fields = ['id_list','mode']
    _before = ['mode', 'init']
    def _check(self):
        if hasattr(self, 'init'):
            ensure(self.init,
                   self.mode._type.synonym,
                   'Declaration {used} does not match initializer {allowed}')
            init_replacement = []
            for _id in self.id_list:
                _id._type = self.init._type
                init_replacement.append(Assignment(_id,
                                                   AssignmentOperator('='),
                                                   self.init))
            self.init = init_replacement
        self._closure.add_list(self.id_list, self.mode._type.synonym)

    def __call__(self):
        for id_ in self.id_list:
            self._code.make_variable(id_.name, self.mode._type)
        if hasattr(self, 'init'):
            self._code += generate(self.init)

class DeclStmt(AST):
    _fields = ['dcl_list']

class Call(AST):
    _fields = ['fname']
    def _check(self):
        self.args = self.args if hasattr(self, 'args') else []
        ensure(self.fname, FuncType)
        self._type = self.fname._type.returns

    def __call__(self):
        # activation record initialization
        arguments = []
        for argument, typearg in zip(getattr(self, 'fargs', []),
                                     self.fname._type.arguments):
            code = argument()
            if issubclass(typearg._type, LocType):
                code.code.pop()
            arguments.append(code)

        self._code += [
            ('alc', self.fname._type.returns.size),
#            generate(getattr(self, 'fargs', [])),
            arguments[::-1],
            ('cfu', self._code.function(self.fname.name))
        ]

# NOT OK. Need to actually implement the builtins.
class BuiltinCall(Call):
    _before = ['fargs']
    def _check(self):
        self._type = NoneType
        if self.fname.name == 'lower':
            ensure(self.fargs[0], (ArrayType,StringType))
            self._type = IntType

        elif self.fname.name == 'upper':
            self._type = IntType

        elif self.fname.name == 'read':
            if any(arg._type.size != 1 for arg in self.fargs):
                error('Arguments to read must have size 1')

        elif self.fname.name == 'print':
            pass

        # uLya
        elif self.fname.name == 'get_string_size':
            assert len(self.fargs) == 1
            self._type = IntType
            ensure(self.fargs, StringType)

        elif self.fname.name == 'push_to_stack':
            assert len(self.fargs) == 1

        elif self.fname.name == 'load_string_contents':
            assert len(self.fargs) == 1
            ensure(self.fargs, StringType)

        elif self.fname.name == 'store_from_stack':
            assert len(self.fargs) == 1

        elif self.fname.name == '__jump__':
            assert len(self.fargs) == 1
            ensure(self.fargs[0], StringType)

        elif self.fname.name == '__jump_on_false__':
            assert len(self.fargs) == 2
            ensure(self.fargs[0], StringType)

    def __call__(self):
        if self.fname.name == 'lower':
            if issubclass(self.fargs[0]._type, StringType):
                self._code += ('ldc', 0)
            else:
                self._code += ('ldc', self.fargs[0]._type.dims[0][0])

        elif self.fname.name == 'upper':
            if issubclass(self.fargs[0]._type, StringType):
                self._code += ('ldc', self.fargs[0]._type.size)
            else:
                self._code += ('ldc', self.fargs[0]._type.dims[0][1])

        elif self.fname.name == 'read':
            for arg in self.fargs:
                if issubclass(arg._type, StringType):
                    self._code += arg()
                    self._code.code.pop()
                    self._code += ('rds')
                else:
                    self._code += arg()
                    self._code.code.pop()
                    self._code += ('rdv')
                    self._code += ('smv', 1)
            
        elif self.fname.name == 'print':
            for arg in self.fargs:
                self._code += arg()
                if issubclass(arg._type, StringType):
                    last_op = self._code.code.pop()
                    if last_op[0] == 'sts':
                        new_op = ('prc', last_op[1])
                        self._code += new_op
                    else:
                        self._code += ('prs')
                else:
                    self._code += ('prt', arg._type.size)

        # uLya
        elif self.fname.name == 'get_string_size':
            self._code += self.fargs[0]()
            size = self._code.code.pop()[1]
            self._code += ('lmv',1)

        elif self.fname.name == 'push_to_stack':
            self._code += self.fargs[0]()

        elif self.fname.name == 'load_string_contents':
            self._code += self.fargs[0]()
            size = self._code.code.pop()[1]
            self._code += [
                    # Addr is the last thing on stack
                    ('ldc', 1),
                    ('add'),
                    ('lmv', size-1)
                    ]
        elif self.fname.name == 'store_from_stack':
            var = self._code.variable(self.fargs[0].name)
            self._code += [
                    ('ldr', var),
                    ('smv', self.fargs[0]._type.size-1)
                    ]

        elif self.fname.name == '__jump__':
            self._code += ('jmp', self.fargs[0].data)

        elif self.fname.name == '__jump_on_false__':
            self._code += self.fargs[1]()
            self._code += ('jof', self.fargs[0].data)

class BuiltinName(Id):
    _fields = ['name']

class StringMode(IntegerFields):
    _fields = ['length']
    def _check(self):
        super()._check()
        if not isinstance(self.length, Primitive):
            error('String length in declaration must be constant')
        self._type = TypeFactory.string(self.length.data)

class ArrayMode(AST):
    _fields = ['index_mode','element_mode']
    def _check(self):
        #TODO: Check with the professor about index_mode being a discrete_mode, 
        #it doesn't make much sense because anything used to index must be an
        #int
        ranges = []
        for index in self.index_mode:
            lit_range = index
            if isinstance(index, DiscreteMode):
                ensure(index, DiscreteRangeMode);
                lit_range = index.literal_range
            ranges.append((lit_range.lower_bound.data,
                           lit_range.upper_bound.data))
        self._type = TypeFactory.array(self.element_mode._type.synonym,
                                       ranges)

class LiteralRange(IntegerFields):
    _fields = ['lower_bound','upper_bound']
    def _check(self):
        super()._check()

class DiscreteRangeMode(AST):
    '''This should only be used for Range iteration'''
    _fields = ['discrete_mode','literal_range']
    def _check(self):
        ensure(self.discrete_mode, ModeType)
        self._type = TypeFactory.ref(self.discrete_mode._type)

class Action(AST):
    _fields = ['action']
    _after = 'all'
    _before = 'none'
    def _check(self):
        if hasattr(self, 'label_id'):
            self._closure[self.label_id.name] = LabelType

    def __call__(self):
        if hasattr(self, 'label_id'):
            self._code += ('lbl', self._code.make_label(self.label_id.name))
            after_label = self._code.make_label('*after*'+self.label_id.name)
        self._code += self.action()
        if hasattr(self, 'label_id'):
            self._code += ('lbl', after_label)

class TopLevelAction(Passthrough):
    '''This class exists to cleanup function calls in statements'''
    _fields = ['action']
    def __call__(self):
        self._code += self.action()
        if isinstance(self.action, ProcedureCall):
            self._code += ('dlc', self._type.size)

class Exit(AST):
    _fields = ['label']
    def __call__(self):
        self._code += ('jmp', self._code.label('*after*'+self.label.name))

class DiscreteMode(AST):
    _fields = ['mode']
    def _check(self):
        if isinstance(self.mode, str):
            if self.mode == 'bool':
                self._type = BooleanType
            else:
                self._type = class_from_name(self.mode.capitalize())
            self._type = TypeFactory.mode(self._type)
        # If composite type, remove wrapper
        else:
            self._replacement = self.mode

class ReferenceMode(AST):
    _fields = ['ref_mode']
    def _check(self):
        self._type = TypeFactory.ref(self.ref_mode._type.synonym)

# Not ok, handle composites
class Assignment(AST):
    _fields = ['variable','operator','expression']
    # if operator is not '=', it will unroll to
    # variable = variable `op` expression
    def _check(self):
        # Unpack the operator
        if self.operator.operator == '=':
            ensure(self.variable, self.expression._type)
        else:
            op = self.operator.operator[0]
            operation_results = self.variable._type.bops[op]
            try:
                self._type = operation_results[self.expression._type]
            except KeyError:
                error('Operation %s not defined for '
                      '%s and %s'%(op,
                                   self.variable._type,
                                   self.expression._type))
            # Replace the composite assignment to an assigment after the
            # expression
            op = BinaryOperation(self.variable,
                                 Operator(self.operator.operator[0]),
                                 self.expression)
            self.expression = op
            self.operator = '='
        # Use a dummy return type, just like in rust
        self._type = NoneType

    def __call__(self):
        self._code += self.variable()
        if not isinstance(self.variable, Call):
            self._code.code.pop()
        self._code += self.expression()
        if self._code.code[-1][0] is not 'sts':
            self._code += ('smv', self.variable._type.size)

class LocMode(AST):
    _fields = ['referenced']
    def _check(self):
        self._type = TypeFactory.mode(
                        TypeFactory.loc(
                            self.referenced._type.synonym))

class ProcedureStatement(AST):
    _fields = ['label','definition']
    _before = 'none'
    _after = ['definition']
    def _check(self):
        self.definition._fname = self.label.name

    def __call__(self):
        # Ensures that the outer scope won't fall into the function
        guard = self._code.make_label()
        self._code.make_function(self.label.name)
        self._code += [
            ('jmp', guard),
            ('fnmark', self._code.function(self.label.name)),
            self.definition(),
            ('lbl', guard)
        ]

class Conditional(AST):
    _fields = ['cond','then','else_expr']
    _order = ['cond','then','elsif','else_expr']
    def _check(self):
        # Type checking
        ensure(self.cond, BooleanType);
        ensure(self.then, self.else_expr._type)
        if hasattr(self, 'elsif'):
            for elsif_expr in self.elsif:
                ensure(self.then, elsif_expr._type)
        self._type = self.then._type

        reachable = []
        for condition in getattr(self, 'elsif', []):
            # constant condition
            if condition._const:
                # true shortcircuit, promote that condition to else and stop
                if condition.cond.data:
                    self.else_expr = condition.body
                    break
                # false shortcircuit, remove from the tree
                else:
                    pass
            else:
                reachable.append(condition)

        # Constexpr evaluation
        if self.cond._const:
            if self.cond.data:
                self._replacement = self.then
            elif not reachable:
                self._replacement = self.else_expr
            else:
                self.then = reachable[0]
                self.elsif = reachable[1:]
        else:
            self.elsif = reachable

    def __call__(self):
        false_path = self._code.make_label()
        end = self._code.make_label('end')
        # First condition path
        self._code += [
                self.cond(),
                ('jof', false_path),
                self.then(),
                ('jmp', end)
                ('lbl', false_path),
        ]
        for elsif in self.elsif:
            self._code += elsif()
        # False path
        self._code += [
                self.else_expr(),
                ('lbl', end)
        ]

class Return(AST):
    _fields = ['data']
    def _check(self):
        if self.data is not None:
            ensure(self.data, self._closure['*return*'])
        self._type = NoneType

    def __call__(self):
        fno = self._code.function(self._closure['*temp_fname*'])
        stack, pos = self._code.variable('*return*')
        if self.data is not None:
            self._code += self.data()
            self._code += ('stv', stack, pos)
        self._code += ('dlc', self._code.loc_var_mem())
        self._code += ('ret', fno, self._code.argsize())

class Result(AST):
    _fields = ['data']
    def _check(self):
        ensure(self.data, self._closure['*return*'])
        # Use a dummy return type, just like in rust
        self._type = NoneType

    def __call__(self):
        self._code += self.data()
        ret_type = self._closure['*return*'];
        if (issubclass(ret_type, LocType) and
            not isinstance(self.data, Call)):
            self._code.code.pop()
        self._code += ('stv', self._code.variable('*return*'))

class Location(AST):
    _fields = ['location']
    def _check(self):
        # locations are never const
        if isinstance(self.locations, Primitive):
            error("Cannot take location of synonyom")
        self._type = TypeFactory.ref(self.location._type)

    def __call__(self):
        # TODO: figure out how to do references for multiple stuff
        self._code += self.location()
        self._code.code.pop()

class DeRef(AST):
    _fields, _field_types = zip(*[
        ('location','Ref')
    ])
    def _check(self):
        self._type = self.location._type.referenced

    def __call__(self):
        self._code += ('lmv', 1)

class Then(Passthrough):
    _fields = ['body']

class Else(Passthrough):
    _fields = ['body']

class Elsif(AST):
    _fields, _field_types = zip(*[
        ('cond','Bool'),
        ('body','Any')
    ])
    def _check(self):
        self._type = self.body._type
        self._const = self.cond._const
        self._value = self.cond.data

    def __call__(self):
        false_path = self._code.make_label()
        end = self._code.label('end')
        self._code += [
                self.cond(),
                ('jof', false_path),
                self.body(),
                ('jmp', end),
                ('lbl', false_path),
        ]

class ProcedureDefinition(AST):
    _fields = ['statements']
    _order = ['params','result_spec','statements']
    _before = ['params', 'result_spec']
    _after = ['statements']

    def _check(self):
        if hasattr(self, 'result_spec'):
            rspec = self.result_spec._type.synonym
        else:
            rspec = NoneType
        self._closure['*temp_fname*'] = self._fname
        self._closure[self._fname] = TypeFactory.function(
                flatten([x.id_list
                         for x
                         in getattr(self, 'params', [])]),
                rspec)
        self._closure = self._closure.copy()
        self._closure['*return*'] = rspec

        self._new_closure = True

    def __call__(self):
        # Generating for the parameters is actually reserving the labels
        #self._new_closure = True
        fname = self._fname
        self._code.begin_function(fname)
        self._code += ('enf', self._code.function(fname))
        for arg in getattr(self, 'params', []):
            # Lend the code object to leak closure
            arg._code = self._code
            arg()
        if hasattr(self, 'result_spec'):
            self._code.add_argument('*return*', self.result_spec._type.size)
        else:
            self._code.add_argument('*return*', 1)
        code = [statement() for statement in self.statements]
        fno = self._code.function(fname)
        self._code += ('alc', self._code.loc_var_mem())
        self._code += code
        self._code += ('dlc', self._code.loc_var_mem())
        self._code += ('ret', fno, self._code.argsize())

class IfAction(AST):
    _fields, _field_types = zip(*[
        ('cond','Bool'),
        ('body','Any')
    ])

    def __call__(self):
        false_path = self._code.make_label()
        end = self._code.make_label('end')
        # First condition path
        self._code += [
                self.cond(),
                ('jof', false_path),
                [b() for b in self.body],
                ('jmp', end),
                ('lbl', false_path),
        ]
        # False path
        if hasattr(self, 'else_clause'):
            # There are some elsifs and an else here
            for clause in self.else_clause:
                self._code += clause()
        self._code += ('lbl', end)

class ContainerAccess(AST):
    _fields, _field_types = zip(*[
        ('location', ['Array', 'Ref']),
        ('expressions', 'Int')
    ])
    def _check(self):
        if len(self.expressions) != len(self.location._type.dims):
            error("Wrong number of dimensions at access")
        self._type = self.location._type.referenced

    def __call__(self):
        element_size = self.location._type.referenced.size

        # values that are going to used to calculate offsets
        # [a:A,b:B,c:C,d:D]
        # a*B*C*D + b*C*D + c*D + d
        dims = self.location._type.dims
        tot_dim = reduce(lambda x,y: x*y[1], dims, 1)
        mul_dims = []
        for ld, ud in dims:
            mul_dims.append(tot_dim/ud)
            tot_dim /= ud

        self._code += self.location()
        self._code.code.pop()

        # ctr = 0
        self._code += ('ldc', 0)
        for expr, offset, base in zip(self.expressions, mul_dims, dims):
            self._code += expr()
            self._code += ('ldc', base[0])
            self._code += ('sub')
            # e-base
            self._code += ('idx', int(offset*element_size))
            # ((e-base)*offset)+ctr
        self._code += ('add')
        self._code += ('lmv', 1)

# This actually returns the whole slice, this can basically be only copied and
# printed
class ContainerSlice(AST):
    _fields, _field_types = zip(*[
        ('location', 'Array'),
        ('lower_bound', 'Int'),
        ('upper_bound', 'Int')
    ])
    def _check(self):
        if (not isinstance(self.lower_bound, Primitive) or
            not isinstance(self.upper_bound, Primitive) or
            # upper_bound must be lower_bound
            self.lower_bound.data > self.upper_bound.data):
            error('Bounds for slice must be constant')
        self._type = TypeFactory.array(self.location._type.referenced,
                                       [(self.lower_bound.data,
                                         self.upper_bound.data)])
    def __call__(self):
        dims = self._type.dims[0]

        self._code += self.location()
        self._code.code.pop()
        self._code += ('lmv', self._type.size)

class AccessPrimitive(ContainerAccess):
    def __call__(self):
        super().__init__()
        self._code += ('lmv', 1)

class SlicePrimitive(ContainerSlice):
   def __call__(self):
       self._code += self.location()

class Synonym(AST):
    _fields = ['id_list','const_expr']
    _before = ['const_expr', 'mode']

    def _check(self):
        #TODO: best error msg
        if not self.const_expr._const:
            error('Expression should be constant')
        if hasattr(self, 'mode'):
            ensure(self.const_expr, self.mode._type.synonym)
        # Assigning to hard values, see how it plays out
        for var in self.id_list:
            self._closure[var.name] = self.const_expr

    def __call__(self):
        '''Do nothing here'''
        pass

class ModeDefinition(AST):
    _fields = ['id','mode']
    _before = ['mode']

    def _check(self):
        for var in self.id:
            self._closure[var.name] = self.mode._type.synonym

    def __call__(self):
        '''Do nothing here'''
        pass

class FormalParameter(AST):
    _fields = ['id_list', 'parameter_spec']
    _before = ['parameter_spec']
    def _check(self):
        for id_ in self.id_list:
            id_._type = self.parameter_spec._type.synonym
        self._closure.add_list(self.id_list, self.parameter_spec._type.synonym)

    def __call__(self):
        for id_ in self.id_list:
            self._code.add_argument(id_.name, self.parameter_spec._type.size)

class ElseClause(AST):
    _fields = ['body']

class ElsifClause(AST):
    _fields, _field_types = zip(*[
        ('cond', 'Bool'),
        ('body', 'Any')
    ])

    def __call__(self):
        false_path = self._code.make_label()
        end = self._code.label('end')
        self._code += [
                self.cond(),
                ('jof', false_path),
                self.body(),
                ('jmp', end),
                ('lbl', false_path)
        ]

class BinaryOperation(AST):
    _fields = ['la','op','ra']
    _after = ['complex']
    def _check(self):
        op = self.op.operator
        if isinstance(self.la, LocType):
            self.la = self.la.underlying

        if isinstance(self.ra, LocType):
            self.ra = self.ra.underlying
        # Determination of constat expressions
        self._const = self.la._const and self.ra._const

        if issubclass(self.la._type, StringType):
            left_type = StringType
        else:
            left_type = self.la._type

        if issubclass(self.ra._type, StringType):
            right_type = StringType
        else:
            right_type = self.ra._type

        operation_results = left_type.bops[op]

        try:
            self._type = operation_results[right_type]
        except KeyError:
            error('Operation %s not defined for '
                  '%s and %s'%(op, self.la._type, self.ra._type))

        if self._const:
            # Python3 integer division
            if op == '/':
                op = '//'
            elif op == '&':
                op = '+'

            value = eval(str(self.la.data) + op + str(self.ra.data))
            constclass = simple_lya_to_node(self._type)
            self._replacement = constclass(value)

        elif (op == '&' or
              op == 'in' or
              issubclass(self.la._type,StringType)):
            la_sub, ra_sub = random_name(), random_name()
            if op == '&':
                self._type = TypeFactory.string(self.la._type.size+
                                                self.ra._type.size-2)
                prog = '''
                       dcl {ctr} int;
                       push_to_stack(get_string_size({s1}) +
                                     get_string_size({s2}));
                       load_string_contents({s1});
                       do for {ctr} = get_string_size({s1}) to upper({s1})-2;
                           ?('dlc', 1)?;
                       od;
                       load_string_contents({s2});
                       do for {ctr} = {ctr} down to get_string_size({s1})+1;
                           ?('alc', 1)?;
                       od;
                       '''.format(s1=la_sub,
                                  s2=ra_sub,
                                  ctr=random_name())
                self.complex = parser.ParseString(prog).statements

            elif op.lower() == 'in':
                self._type = BooleanType
                prog = '''
                       dcl {flag} bool = false;
                       dcl {ctr} int;
                       do for {ctr} = 1 to get_string_size({ra});
                           {flag} = {flag} || ({ra}[{ctr}] == {la});
                       od;
                       push_to_stack({flag});
                       '''.format(la=la_sub,
                                  ra=ra_sub,
                                  ctr=random_name(),
                                  flag=random_name())
                self.complex = parser.ParseString(prog).statements
            elif op == '==' or op == '!=':
                self._type = BooleanType
                prog = '''
                    dcl {flag} bool = true;
                    dcl {ctr} int;
                    do for {ctr} = 0 to 
                            if get_string_size({la}) > get_string_size({la})
                            then get_string_size({la})
                            else get_string_size({ra}) fi;
                        {flag} = {flag} && ({la}[{ctr}] {op} {ra}[{ctr}]);
                    od;
                    push_to_stack({flag});
                '''.format(la=la_sub,
                           ra=ra_sub,
                           ctr=random_name(),
                           flag=random_name(),
                           op=op)
                self.complex = parser.ParseString(prog).statements
            replace_id(self.complex, la_sub, self.la)
            replace_id(self.complex, ra_sub, self.ra)


    def __call__(self):
        op = self.op.operator
        op_to_instruction = {
            '+': ('add'),
            '-': ('sub'),
            '/': ('div'),
            '*': ('mul'),
            '%': ('mod'),
            '||':('lor'),
            '&&':('and'),
            '<': ('les'),
            '>': ('grt'),
            '<=':('leq'),
            '>=':('gre'),
            '==':('equ'),
            '!=':('neq')
        }
        # Cannot test if instance of Primitive because of the in operator
        if op == '&' or op.lower() == 'in':
            self._code += generate(self.complex)
        else:
            # Gets interpreted normally as simple instructions
            self._code += self.la()
            self._code += self.ra()
            self._code += op_to_instruction[op]
            self._const = False

class MonadicOperator(AST):
    _fields = ['op','la']
    def _check(self):
        op = self.op
        operation_results = self.la._type.mops[op]
        if isinstance(self.la, LocType):
            self.la = self.la.underlying

        # Constant expression determination
        if self.la._const:
            self._value = eval(self.op + str(self.la.data))
            self._const = True
        else:
            self._const = False

        if issubclass(operation_results, AnyType):
            self._type = operation_results
        else:
            error('Operation %s not defined for %s'%(op, self.la._type))

    def __call__(self):
        self._code += self.la()
        if self.op == '!':
            self._code += ('not')
        elif self.op == '-':
            self._code += ('neg')

class IntegerExpression(Expression):
    _fields, _field_types = zip(*[
        ('op_tree', 'Int'),
    ])

class ProcedureCall(Call):
    pass

class Range(AST):
    _fields = ['loop_counter','discrete_mode','direction']
    _after = 'all'
    _before = ['discrete_mode']
    def _check(self):
        counter_type = self.discrete_mode._type.referenced
        self._closure[self.loop_counter.name] = counter_type

    def __call__(self):
        ctr = self._code.variable(self.loop_counter.name)
        # This is the place that is generated after a continue statement
        loop_back = self._code.label('continue')
        end = self._code.label('end')
        skip_increment = self._code.make_label()

        # Bounds for the discrete mode
        lower = self.discrete_mode.literal_range.lower_bound()
        upper = self.discrete_mode.literal_range.upper_bound()

        # Add 1 or -1 to correct for the next increment step
        self._code += [
                lower,
                ('stv', ctr),
                ('jmp', skip_increment),
                ('lbl', loop_back),
                ('ldv', ctr),
                ('ldc', 1),
                ('add' if self.direction == 'up' else 'sub'),
                ('stv', ctr),
                ('lbl', skip_increment),
                ('ldv', ctr),
                upper,
                ('les'),
        ]
#---------------------------End of useful classes------------------------------
#---------------------------Start call trickery--------------------------------

def add_wrapper_to_AST_calls():
    goebbels = globals().copy()
    for classobj in goebbels.values():
        # Only changing AST objects
        if (not isinstance(classobj, type) or
            not issubclass(classobj, AST) or
            # Program is set up from outside
            issubclass(classobj, Program)):
            continue
        def foo(classobj):
            # Get the stack frame of the caller
            oldcall = classobj.__call__
            def newcall(self):
                caller_frame = sys._getframe().f_back
                while 'self' not in caller_frame.f_locals:
                    caller_frame = caller_frame.f_back
                parent = caller_frame.f_locals['self']
                # Steal the code object from parent unless stated. Copying the
                # code object.
                self._code = (parent._code.deepcopy()
                              if hasattr(self, '_new_closure') else
                              parent._code.copy())
                oldcall(self)
                return self._code
            classobj.__call__ = newcall
        foo(classobj)

add_wrapper_to_AST_calls()

#---------------------------End of call trickery-------------------------------
class Operator(AST):
    _fields = ['operator']
    def __init__(self,operator):
        if isinstance(operator,Operator):
            self.operator = operator.operator
        else:
            self.operator = operator

class AssignmentOperator(Operator):
    pass

class Walker(object):
    def __init__(self):
        self.default_order = 'before'

    def walk(self, node, parent):
        if isinstance(node, list):
            self.walk_children(node, parent=parent)
        elif isinstance(node, AST):
            node._closure = parent._closure
            if node._before == 'all':
                self.walk_children(node)
            elif isinstance(node._before, list):
                for child in node._before:
                    if hasattr(node, child):
                        self.walk(getattr(node, child), node)
            self.method(node, parent)
            if node._after == 'all':
                self.walk_children(node)
            elif isinstance(node._after, list):
                for child in node._after:
                    if hasattr(node, child):
                        self.walk(getattr(node, child), node)


    def walk_children(self, node, parent=None):
        # Pass parent as an argument to preserve the closure in case of list
        # elements
        if parent is None:
            parent = node
        if isinstance(node, list):
            for c in node:
                self.walk(c, parent)
        elif isinstance(node, AST):
            if hasattr(node,'_order'):
                for c in [getattr(node,f)
                        for f
                        in node._order
                        if hasattr(node,f)]:
                    self.walk(c, parent)
            else:
                for c in [getattr(node,f) for f in node._fields]:
                    self.walk(c, parent)
                for k,v in node.__dict__.items():
                    if k not in node._fields and k[0] != '_':
                        self.walk(v, parent)

class StringMem(object):
    strings = {}
    ctr = 0
    def add(string):
        StringMem.strings[string] = StringMem.ctr
        StringMem.ctr += 1
