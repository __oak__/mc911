import sys
import os
from functools import reduce
from collections import defaultdict
from pprint import pprint
from IPython import embed
from inspect import isclass
import random

def flatten(listoflists):
    return reduce(lambda x,y: x+y, listoflists, [])

def flatten_directories(fname):
    if os.path.isdir(fname):
        return flatten([flatten_directories(fname+f)
                        for f
                        in os.listdir(fname)])
    elif os.path.isfile(fname):
        return [fname]
    else:
        return []

def get_files_in_args(arglist):
    return flatten([flatten_directories(arg) for arg in sys.argv[1:]])

def random_name():
    return '_%d'%random.randint(0,10000)
